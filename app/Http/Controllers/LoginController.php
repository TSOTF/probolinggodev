<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;
class LoginController extends Controller
{


    protected function outputJSON($result = null, $message = '', $responseCode = 200) {

    if ($message != '') $response["message"] = $message;
    if ($result != null) $response["result"] = $result;

    return response()->json(
        $response, 
        $responseCode);
    }

    public function login(Request $request)
    {
     
        $user = User::where('email',$request->input('email'))->first();
        if($user == null) {
            return $this->outputJSON(null,"Incorrect username / password",404);
        } elseif (Hash::check($request->input('password'), $user->getAuthPassword())) {
            $user->api_token = str_random(60);
            $user->save();
            return $this->outputJSON($user,"Logged In Successfully");
        } else {
            return $this->outputJSON(null,"Incorrect username / Password",404);         
        }   
    }

        /*
    * This function will get the authenticated user
    * unset and save the api token
    */
    public function logout()
    {
        $user = Auth::user();
        $user->api_token = null;
        $user->save();
        return $this->outputJSON(null,"Successfully Logged Out"); 
    }
}
