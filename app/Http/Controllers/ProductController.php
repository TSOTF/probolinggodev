<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return Product::where('votes', '>', 100)->paginate(15);
        //        $filter = (isset($_GET['filter'])) ? $_GET['filter'] : false;
        // $search = (isset($_GET['search'])) ? $_GET['search'] : false;
        $filter = $request->input('filter');
        $search = $request->input('search');
        $product = Product::whereRaw('1 = 1');
        if (!empty($filter)){
            // return [2=>$filter];
            $product->where('category', '=', $filter);
        }
        if (!empty($search)){
            // return [2=>$search];
            $product->where('name', 'like', '%'.$search.'%');
        }

        if($search){

        }
        //
        return $product->paginate(10);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->category = $request->input('category');
        $product->save();

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

      $product = Product::find($request->input('id'));
      $product->name = $request->input('name');
      $product->description = $request->input('description');
      $product->category = $request->input('category');
      if ($product->update()){
        return $product;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product){
            $product->delete();
            return ["status"=>"success"];
        }else{
            return ["message"=>"data not found"];
        }
    }
}
