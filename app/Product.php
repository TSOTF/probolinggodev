<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'Products';
    protected $primaryKey = 'id';
    protected $filable = [
			'name',
			'description',
			'category'];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
	
}
