/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.16-MariaDB : Database - probolinggodev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_02_09_074908_create_products_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '''UNCATEGORY''',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`description`,`category`,`created_at`,`updated_at`) values (1,'Mie ayam','Keterangan mie ayam','MAKANAN','2018-02-09 15:17:51','2018-02-09 15:17:53'),(2,'Mie sapi','Keterangan mie ayam','MAKANAN','2018-02-09 15:17:51','2018-02-09 15:17:53'),(3,'Jus Mangga','Keterangan Jus Mangga','MINUMAN','2018-02-09 15:17:51','2018-02-09 15:17:53'),(4,'Jus apel','Keterangan jus apel','MINUMAN','2018-02-09 15:18:50','2018-02-08 15:18:55'),(5,'sue','deskripsi','MAKANAN','2018-02-09 10:17:01','2018-02-09 10:17:01'),(6,'sue','deskripsi','MAKANAN','2018-02-09 10:17:24','2018-02-09 10:17:24'),(7,'sue','deskripsi','MAKANAN','2018-02-09 10:17:26','2018-02-09 10:17:26'),(8,'sue',NULL,NULL,'2018-02-09 10:17:33','2018-02-09 10:17:33'),(9,'KELAPA MUDA','deskripsi','MINUMAN','2018-02-09 10:17:57','2018-02-09 10:43:24');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`api_token`,`remember_token`,`created_at`,`updated_at`) values (1,'Miss Kylee Rippin','goyette.theodora@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',NULL,'iHqYYAW2EG','2018-02-09 07:59:37','2018-02-09 12:27:41'),(2,'Miss Zoila Aufderhar IV','audrey.funk@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','rvjnTkCAELcEi3S0IA7Au1DCAL1mlaZshmMDi7KPRqA5gblUJY8HT2eBieVM','fzfODhsLsh','2018-02-09 07:59:37','2018-02-09 07:59:37'),(3,'Marta Parker','frunolfsson@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Nv7NjabONE1A0qZYxQAS0fhUBa2WhPJKwHtPIkZ1c7dBGunr3AhtK1AHfOBX','3cnbvcuPnn','2018-02-09 08:10:38','2018-02-09 08:10:38'),(4,'Prof. Burnice Kuphal','rau.marianne@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','LTGMvS0gZxOThNanNJhRljfLjRiMR7ZTlirUAPOI57W45Lygs5U4rqg2dR1d','tXDlaEjXJi','2018-02-09 08:10:38','2018-02-09 08:10:38');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
