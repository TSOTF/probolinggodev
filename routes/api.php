<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('product', 'ProductController', 
// [
//  //'except' => ['edit'],
//  //'middleware' => ['auth:api']
// ]);

// Route::resource('product', 'ProductController', 
// [
//  'except' => ['create', 'edit'],
//  'middleware' => ['auth:api']
// ]);
// 

// Route::get('product', 'ProductController@index', 
// [
//  'middleware' => ['auth:api']
// ]);
Route::get('logout', 'LoginController@logout')->middleware('auth:api');
Route::post('login', 'LoginController@login');


Route::group(['middleware' => ['auth:api']], function () {
	Route::get('backend/product','ProductController@index');
	Route::get('backend/product/{id}','ProductController@show', function ($id) {})->where('id', '[0-9]+');
	Route::post('backend/product','ProductController@store');
	Route::post('backend/product/update','ProductController@update');
	Route::delete('backend/product/delete/{id}','ProductController@destroy', function ($id) {})->where('id', '[0-9]+');
});


Route::get('product','ProductController@index');
Route::get('product/{id}','ProductController@show', function ($id) {})->where('id', '[0-9]+');
Route::post('product','ProductController@store');
Route::post('product/update','ProductController@update');
Route::delete('product/delete/{id}','ProductController@destroy', function ($id) {})->where('id', '[0-9]+');

